/*
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
*
* Maciej Andziński <maciej.andzinski@nic.cz>
*
* 2018.09
*
* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
*/

/*
*
* Spark UDF to compute Euclidean distance between two vectors
*
*/

package adamrr

import org.apache.spark.sql.SparkSession
import org.apache.spark.ml.linalg.Vector
import org.apache.spark.ml.linalg.Vectors

import scala.math.sqrt

object Main {

  def register_udf_distance(spark: SparkSession) = {
    spark.udf.register("udf_distance", (v1: Vector, v2: Vector) => {
      sqrt(Vectors.sqdist(v1,v2))
    })
  }

}


