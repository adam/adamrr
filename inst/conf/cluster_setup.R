# ---- spark connection setup ----

Sys.setenv(HADOOP_CONF_DIR = "/etc/hadoop/conf")
Sys.setenv(HADOOP_USER_NAME = "impala")
Sys.setenv(SPARK_HOME = "/opt/cloudera/parcels/SPARK2-2.3.0.cloudera2-1.cdh5.13.3.p0.316101/lib/spark2")
Sys.setenv(SCALA_HOME = "/usr/share/scala-2.11")
Sys.setenv(JAVA_HOME = "/usr/lib/jvm/java-8-openjdk-amd64/")

config <- sparklyr::spark_config()
config$spark.driver.cores <- 2
config$spark.executor.instances <- 6
config$spark.driver.memory <- "8g"
config$spark.executor.memory <- "16g"
config$spark.sql.parquet.binaryAsString = TRUE

config$spark.sql.shuffle.partitions = 2
